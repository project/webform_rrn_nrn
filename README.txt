INTRODUCTION
------------

Provides webform fieldtype for the Belgian National Insurance Number
(rijksregisternummer/numéro de registre national).

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/webform_rrn_nrn

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/webform_rrn_nrn

REQUIREMENTS
------------

This module requires the following modules:

* Webform (https://www.drupal.org/project/webform)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit:
  https://www.drupal.org/documentation/install/modules-themes/modules-8
  for further information.

CONFIGURATION
-------------

* Add this element to a webform in Structure » Webforms » your webform » Build:

  - Click on +Add element

    This Element can be found in the Custom category
    You can also add a custom error message.
